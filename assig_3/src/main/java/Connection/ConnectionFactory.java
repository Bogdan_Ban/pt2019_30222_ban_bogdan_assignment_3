package Connection;

import java.sql.*;
import java.util.logging.Logger;
import java.sql.SQLException;

public class ConnectionFactory {
	
	private static final Logger LOGGER = Logger.getLogger(ConnectionFactory.class.getName());
	private static final String Driver = "com.mysql.cj.jdbc.Driver";
	private static final String DBurl = "jdbc:mysql://localhost:3306/schooldb";
	private static final String User = "root";
	private static final String Pass = "root";
	private static Connection conn; 
	
	private static ConnectionFactory singleInstance;
	
	private ConnectionFactory() throws SQLException {
		try {
			Class.forName(Driver);
			this.conn = DriverManager.getConnection(DBurl, User, Pass);
		}catch(ClassNotFoundException e){
			e.printStackTrace();
		}
	}
	
	public static Connection getConnection() throws SQLException {
		if(singleInstance == null) {
			singleInstance = new ConnectionFactory();
		}
		return conn;
	}
	
	public static void close(Connection connection) {
		try {
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void close(Statement stmt) {
		try {
			stmt.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void close(ResultSet res) {
		try {
			res.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	 public static ConnectionFactory getInstance() throws SQLException {
	        if (singleInstance == null) {
	        	singleInstance = new ConnectionFactory();
	        } else if (singleInstance.getConnection().isClosed()) {
	        	singleInstance = new ConnectionFactory();
	        }
			return singleInstance;
	 }
	
}

