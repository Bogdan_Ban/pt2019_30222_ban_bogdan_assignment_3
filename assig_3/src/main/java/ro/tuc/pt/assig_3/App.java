package ro.tuc.pt.assig_3;

import Model.*;
import presentation.Controller;
import presentation.View;

public class App 
{
    public static void main( String[] args ){
    	View v = new View("Warehouse");
		Controller ctr = new Controller(v);
    }
}
