package DAO;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JTable;
import javax.swing.table.TableColumnModel;

import Connection.*;

public class AbstractDAO<T>{
	protected static final Logger LOGGER = Logger.getLogger(AbstractDAO.class.getName());
	private final Class<T> type;
	@SuppressWarnings("unchecked")
	public AbstractDAO() {
		this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}
	
	public void add(String query) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		System.out.println(query);
		try {
			conn = ConnectionFactory.getConnection();
			stmt = conn.createStatement();
			stmt.executeUpdate(query);
			
		}catch(SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:add " + e.getMessage());
		}finally {
			//ConnectionFactory.close(rs);
			//ConnectionFactory.close(stmt);
			//ConnectionFactory.close(conn);
		}
	}
	
	public void drop(String query) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		System.out.println(query);
		try {
			conn = ConnectionFactory.getConnection();
			stmt = conn.createStatement();
			stmt.executeUpdate(query);
			
		}catch(SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:drop " + e.getMessage());
		}finally {
			//ConnectionFactory.close(rs);
			//ConnectionFactory.close(stmt);
			//ConnectionFactory.close(conn);
		}
	}
	
	public JTable printAll(String tabelName) {
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		String query = "select * from " + tabelName;
		try {
			conn = ConnectionFactory.getConnection();
			stmt = conn.prepareStatement(query);
			rs = stmt.executeQuery();
			
			int len = rs.getMetaData().getColumnCount();
			Vector cols= new Vector(len);
			for(int i=1; i<=len; i++) // Note starting at 1
			    cols.add(rs.getMetaData().getColumnName(i));


			// Add Data
			Vector data = new Vector();
			while(rs.next())
			{
			    Vector row = new Vector(len);
			    for(int i=1; i<=len; i++)
			    {
			        row.add(rs.getString(i));
			    }
			    data.add(row);
			}

			// Now create the table
			JTable table = new JTable(data, cols);
			TableColumnModel columnModel = table.getColumnModel();
			columnModel.getColumn(1).setPreferredWidth(150);
			columnModel.getColumn(2).setPreferredWidth(150);
			columnModel.getColumn(3).setPreferredWidth(150);

			return table;
		}catch(SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
		}finally {
			//ConnectionFactory.close(rs);
			//ConnectionFactory.close(stmt);
			//ConnectionFactory.close(conn);
		}
		return null;
	}
	
	
	public List<T> printTable(String query) {
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		//String query = createSelectQuery(field, id);//System.out.println(query);
		try {
			conn = ConnectionFactory.getConnection();
			stmt = conn.prepareStatement(query);
			//stmt.setInt(1, id);
			rs = stmt.executeQuery();
			
			return createObjects(rs);
		}catch(SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
		}finally {
			//ConnectionFactory.close(rs);
			//ConnectionFactory.close(stmt);
			//ConnectionFactory.close(conn);
		}
		return null;
	}
	
	private List<T> createObjects(ResultSet rs){
		List<T> list = new ArrayList<T>();
		try {
			while(rs.next()) {
				T instance = type.newInstance();
				for(Field field : type.getDeclaredFields()) {
					Object value = rs.getObject(field.getName());
					PropertyDescriptor prop = new PropertyDescriptor(field.getName(), type);
					Method method = prop.getWriteMethod();
					method.invoke(instance, value);
				}
				list.add(instance);
			}
		}catch(InstantiationException e){
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IntrospectionException e) {
			// TODO Auto-generated catch block
			
			e.printStackTrace();
		}
		return list;
	}

	public Class<T> getType() {
		return type;
	}
	
}

