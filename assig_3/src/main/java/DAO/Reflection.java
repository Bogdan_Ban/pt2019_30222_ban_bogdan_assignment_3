package DAO;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.table.TableColumn;

import Model.Clientt;
import Model.Product;

public class Reflection {
	
	public static String retrieveProperties(Object obj) {
		String s = "";
		for(Field f : obj.getClass().getDeclaredFields()) {
			f.setAccessible(true);
			Object val;
			try {
				val = f.get(obj);
				System.out.println(f.getName() + "= " + val);
				s += val + "; ";
			}catch(IllegalArgumentException e){
				e.printStackTrace();
			}catch(IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		return s + "\n";
	}
	
	public static JTable createTableP(List<Product> o2) {
		ArrayList<String> cols = new ArrayList<String>();
		ArrayList<ArrayList<String>> data = new ArrayList<ArrayList<String>>();
		int ok = 0;
		int n,m;
		for(Object o : o2) {
			ArrayList<String> aux = new ArrayList<String>();
			
			for(Field f : o.getClass().getDeclaredFields()) {
				f.setAccessible(true);
				Object val;
				try {
					val = f.get(o);
					if(ok == 0) {
						cols.add(f.getName());
					}
					String str = String.valueOf(val);
					aux.add(str);
				}catch(IllegalArgumentException e){
					e.printStackTrace();
				}catch(IllegalAccessException e){
					e.printStackTrace();
				}
			}
			ok = 1;
			data.add(aux);
		}
		n = data.size();
		m = cols.size();
		String[] vec_cols = new String[m];
		String[][] matr_data = new String[n][m];
		for(int i=0;i<m;i++) {
			vec_cols[i] = cols.get(i);
		}
		for(int p=0;p<n;p++) {
			for(int q=0;q<m;q++) {
				matr_data[p][q] = data.get(p).get(q);
			}
		}
		JTable jtb = new JTable(matr_data,vec_cols);
		return jtb;
	}

	public static JTable createTableC(List<Clientt> o2) {
		ArrayList<String> cols = new ArrayList<String>();
		ArrayList<ArrayList<String>> data = new ArrayList<ArrayList<String>>();
		int ok = 0;
		int n,m;
		for(Object o : o2) {
			ArrayList<String> aux = new ArrayList<String>();
			
			for(Field f : o.getClass().getDeclaredFields()) {
				f.setAccessible(true);
				Object val;
				try {
					val = f.get(o);
					if(ok == 0) {
						cols.add(f.getName());
					}
					String str = String.valueOf(val);
					aux.add(str);
				}catch(IllegalArgumentException e){
					e.printStackTrace();
				}catch(IllegalAccessException e){
					e.printStackTrace();
				}
			}
			ok = 1;
			data.add(aux);
		}
		n = data.size();
		m = cols.size();
		String[] vec_cols = new String[m];
		String[][] matr_data = new String[n][m];
		for(int i=0;i<m;i++) {
			vec_cols[i] = cols.get(i);
		}
		for(int p=0;p<n;p++) {
			for(int q=0;q<m;q++) {
				matr_data[p][q] = data.get(p).get(q);
			}
		}
		JTable jtb = new JTable(matr_data,vec_cols);
		return jtb;
	}
}
