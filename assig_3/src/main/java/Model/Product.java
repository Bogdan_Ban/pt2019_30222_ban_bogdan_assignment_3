package Model;

import DAO.AbstractDAO;

public class Product extends AbstractDAO<Product> {
	
	private int ID_product;
	private String PName;
	private int price;
	private int stock;
	
	public Product() {
		ID_product = 0;
		PName = "";
		price = 0;
	}
	
	public Product(int idP, String n, int p) {
		ID_product =idP;
		PName = n;
		price = p;
	}
	
	public String addProductQuery(String index, String name, String p, String st) {
		StringBuilder sb = new StringBuilder();
		sb.append("insert into product values (" + index + ", '");
		sb.append(name + "', ");
		sb.append(p + ", ");
		sb.append(st + ")");
		return sb.toString();		
	}
	
	public String dropProductQuery(String index) {
		StringBuilder sb = new StringBuilder();
		sb.append("delete from product where ID_product = " + index);
		return sb.toString();		
	}
	
	public int getID_product() {
		return ID_product;
	}
	public void setID_product(int ID_product) {
		this.ID_product = ID_product;
	}
	
	public String getPName() {
		return PName;
	}
	public void setPName(String PName) {
		this.PName = PName;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}
	
}
