package Model;

import DAO.AbstractDAO;

public class OrderItem extends AbstractDAO<OrderItem>{
	
	private int ID_orderItem;
	private int ID_orders;
	private int ID_product;
	private int quantity;
	
	public OrderItem() {
		ID_orderItem = 0;
		ID_orders = 0;
		ID_product = 0;
		quantity = 0;
	}
	
	public OrderItem(int a, int b, int c, int q) {
		ID_orderItem = a;
		ID_orders = b;
		ID_product = c;
		quantity = q;
	}
	
	public String addOrderItemQuery(String index, String ido, String idp, String qua) {
		StringBuilder sb = new StringBuilder();
		sb.append("insert into orderitem values (" + index + ", ");
		sb.append(ido + ", ");
		sb.append(idp + ", ");
		sb.append(qua + ")");
		return sb.toString();		
	}
	
	public String dropOrderItemQuery(String index) {
		StringBuilder sb = new StringBuilder();
		sb.append("delete from orderitem where ID_orderItem = " + index);
		return sb.toString();		
	}
	
	public int getID_orderItem() {
		return ID_orderItem;
	}
	public void setID_orderItem(int ID_orderItem) {
		this.ID_orderItem = ID_orderItem;
	}
	public int getID_orders() {
		return ID_orders;
	}
	public void setID_orders(int ID_orders) {
		this.ID_orders = ID_orders;
	}
	public int getID_product() {
		return ID_product;
	}
	public void setID_product(int ID_product) {
		this.ID_product = ID_product;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
}
