package Model;

import java.sql.*;
import java.util.logging.Level;

import Connection.ConnectionFactory;
import DAO.AbstractDAO;

public class Clientt extends AbstractDAO<Clientt>{
	private int ID_client;
	private String firstName;
	private String Surname;
	private String adress;
	private String email;
	
	public Clientt() {
		ID_client = 0;
		firstName = "";
		Surname = "";
		adress = "";
		email = "";
	}
	
	public Clientt(int id, String f, String s, String adr, String e) {
		ID_client = id;
		firstName = f;
		Surname = s;
		adress = adr;
		email = e;
	}
	
	
	public String addClientQuery(String index, String first, String last, String adr, String email) {
		StringBuilder sb = new StringBuilder();
		sb.append("insert into clientt values (" + index + ", '");
		sb.append(first + "', '");
		sb.append(last + "', '");
		sb.append(adr + "', '");
		sb.append(email + "')");
		return sb.toString();		
	}
	
	public String dropClientQuery(String index) {
		StringBuilder sb = new StringBuilder();
		sb.append("delete from clientt where ID_client = " + index);
		return sb.toString();		
	}
	
	//toString
	public String toString() {
		return (ID_client + " ; " + firstName + " ; " + Surname + " ; " + adress + " ; " + email);
	}
	
	//getters and setters
	public String getSurname() {
		return Surname;
	}
	public void setSurname(String Surname) {
		this.Surname = Surname;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getAdress() {
		return adress;
	}
	public void setAdress(String adress) {
		this.adress = adress;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	public int getID_client() {
		return ID_client;
	}

	public void setID_client(int ID_client) {
		this.ID_client = ID_client;
	}
	
}
