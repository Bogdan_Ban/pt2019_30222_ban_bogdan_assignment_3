package Model;

import DAO.AbstractDAO;

public class Orders extends AbstractDAO<Orders>{
	
	private int ID_order;
	private int ID_client;
	private int totalPrice;
	private int delieveryTime_days;
	
	public Orders() {
		ID_order = 0;
		ID_client = 0;
		totalPrice = 0;
		delieveryTime_days = 0;
	}
	
	public Orders(int id1, int id2, int t, int d) {
		ID_order = id1;
		ID_client = id2;
		totalPrice = t;
		delieveryTime_days = d;
	}
	
	public String addOrderQuery(String index, String idc, String p, String d) {
		StringBuilder sb = new StringBuilder();
		sb.append("insert into orders values (" + index + ", ");
		sb.append(idc + ", ");
		sb.append(p + ", ");
		sb.append(d + ")");
		return sb.toString();		
	}
	
	public String dropOrderQuery(String index) {
		StringBuilder sb = new StringBuilder();
		sb.append("delete from orders where ID_order = " + index);
		return sb.toString();		
	}
	
	public int getID_order() {
		return ID_order;
	}
	public void setID_order(int ID_order) {
		this.ID_order = ID_order;
	}
	public int getID_client() {
		return ID_client;
	}
	public void setID_client(int ID_client) {
		this.ID_client = ID_client;
	}
	public int getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(int totalPrice) {
		this.totalPrice = totalPrice;
	}
	public int getDelieveryTime_days() {
		return delieveryTime_days;
	}
	public void setDelieveryTime_days(int delieveryTime_days) {
		this.delieveryTime_days = delieveryTime_days;
	}
	
}
