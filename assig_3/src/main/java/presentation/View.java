package presentation;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionListener;

import javax.swing.*;

import Model.*;

public class View extends JFrame{
	
	private JPanel pan = new JPanel(new GridBagLayout());
	GridBagConstraints c1 = new GridBagConstraints();
	private JPanel panel = new JPanel(new GridBagLayout());
	GridBagConstraints c = new GridBagConstraints();
	private JTable products = new JTable();
	private JTable clients = new JTable();
	private JTable orderss = new JTable();
	private JTable orderItem = new JTable();
	
	JButton addClient = new JButton("Add Client");
	JTextField idC = new JTextField(30);
	JTextField firstC = new JTextField(30);
	JTextField sureC = new JTextField(30);
	JTextField adrC = new JTextField(30);
	JTextField emailC = new JTextField(30);
	
	JButton dropClient = new JButton("Delete Client");
	JButton addProduct = new JButton("Add Product");
	JTextField idP = new JTextField(30);
	JTextField nameP = new JTextField(30);
	JTextField priceP = new JTextField(30);
	JTextField stockP = new JTextField(30);
	
	JButton dropProduct = new JButton("Delete Product");
	JButton addOrderItem = new JButton("Add order item");
	JButton dropOrderItem = new JButton("Delete order item");
	JTextField idOrderItem = new JTextField(30);
	JTextField idOrd = new JTextField(30);
	JTextField idPro = new JTextField(30);
	JTextField quant = new JTextField(30);
	
	JLabel idCl1 = new JLabel("Client ID");
	JLabel idPro1 = new JLabel("Product ID");
	JLabel quant1 = new JLabel("Quantity");
	JButton createOrder = new JButton("Create order");
	JButton deleteOrder = new JButton("Delete order");
	JTextField idOrder = new JTextField(30);
	JTextField idCll = new JTextField(30);
	JTextField pric = new JTextField(30);
	JTextField days = new JTextField(30);
	
	public View(String name) {
		super(name);
		Product p = new Product();
		Clientt cl = new Clientt();
		Orders or = new Orders();
		OrderItem ordIt = new OrderItem();
		clients = cl.printAll("Clientt");
		products = p.printAll("Product");
		orderss = or.printAll("Orders");
		orderItem = ordIt.printAll("OrderItem");
		
		c1.ipady = 100;
		c1.ipadx = 400;
		c1.gridx = 0;
		c1.gridy = 0;
		panel.add(clients, c1);
		
		c1.ipady = 100;
		c1.ipadx = 400;
		c1.gridx = 1;
		c1.gridy = 0;
		panel.add(products, c1);
		
		c1.ipady = 100;
		c1.ipadx = 200;
		c1.gridx = 2;
		c1.gridy = 0;
		panel.add(orderItem, c1);
		
		c1.ipady = 100;
		c1.ipadx = 200;
		c1.gridx = 4;
		c1.gridy = 0;
		panel.add(orderss, c1);
		///////
		c.ipady = 20;
		c.ipadx = 20;
		c.gridx = 0;
		c.gridy = 1;
		panel.add(addClient, c);
		c.ipady = 20;
		c.ipadx = 20;
		c.gridx = 1;
		c.gridy = 1;
		panel.add(dropClient, c);
		c.ipady = 20;
		c.ipadx = 200;
		c.gridx = 2;
		c.gridy = 1;
		panel.add(idC, c);
		c.ipady = 20;
		c.ipadx = 200;
		c.gridx = 3;
		c.gridy = 1;
		panel.add(firstC, c);
		c.ipady = 20;
		c.ipadx = 200;
		c.gridx = 4;
		c.gridy = 1;
		panel.add(sureC, c);
		c.ipady = 20;
		c.ipadx = 200;
		c.gridx = 5;
		c.gridy = 1;
		panel.add(adrC, c);
		c.ipady = 20;
		c.ipadx = 200;
		c.gridx = 6;
		c.gridy = 1;
		panel.add(emailC, c);
		///////
		
		//////
		c.ipady = 20;
		c.ipadx = 20;
		c.gridx = 0;
		c.gridy = 3;
		panel.add(addProduct, c);
		c.ipady = 20;
		c.ipadx = 20;
		c.gridx = 1;
		c.gridy = 3;
		panel.add(dropProduct, c);
		c.ipady = 20;
		c.ipadx = 200;
		c.gridx = 2;
		c.gridy = 3;
		panel.add(idP, c);
		c.ipady = 20;
		c.ipadx = 200;
		c.gridx = 3;
		c.gridy = 3;
		panel.add(nameP, c);
		c.ipady = 20;
		c.ipadx = 200;
		c.gridx = 4;
		c.gridy = 3;
		panel.add(priceP, c);
		c.ipady = 20;
		c.ipadx = 200;
		c.gridx = 5;
		c.gridy = 3;
		panel.add(stockP, c);
		///////
		c.ipady = 20;
		c.ipadx = 20;
		c.gridx = 0;
		c.gridy = 5;
		panel.add(addOrderItem, c);
		
		c.ipady = 20;
		c.ipadx = 20;
		c.gridx = 1;
		c.gridy = 5;
		panel.add(dropOrderItem, c);
		
		c.ipady = 20;
		c.ipadx = 200;
		c.gridx = 2;
		c.gridy = 5;
		panel.add(idOrderItem, c);
		
		c.ipady = 20;
		c.ipadx = 200;
		c.gridx = 2;
		c.gridy = 6;
		panel.add(idPro, c);
		
		c.ipady = 20;
		c.ipadx = 200;
		c.gridx = 3;
		c.gridy = 5;
		panel.add(idOrd, c);
		
		c.ipady = 20;
		c.ipadx = 200;
		c.gridx = 3;
		c.gridy = 6;
		panel.add(quant, c);
		
		c.ipady = 20;
		c.ipadx = 200;
		c.gridx = 4;
		c.gridy = 5;
		panel.add(idPro, c);
		
		c.ipady = 20;
		c.ipadx = 200;
		c.gridx = 5;
		c.gridy = 5;
		panel.add(quant, c);
		
		c.ipady = 20;
		c.ipadx = 20;
		c.gridx = 0;
		c.gridy = 6;
		panel.add(createOrder, c);
		
		c.ipady = 20;
		c.ipadx = 20;
		c.gridx = 1;
		c.gridy = 6;
		panel.add(deleteOrder, c);
		
		c.ipady = 20;
		c.ipadx = 200;
		c.gridx = 2;
		c.gridy = 6;
		panel.add(idOrder, c);
		
		c.ipady = 20;
		c.ipadx = 200;
		c.gridx = 3;
		c.gridy = 6;
		panel.add(idCll, c);
		
		c.ipady = 20;
		c.ipadx = 200;
		c.gridx = 4;
		c.gridy = 6;
		panel.add(pric, c);
		
		c.ipady = 20;
		c.ipadx = 200;
		c.gridx = 5;
		c.gridy = 6;
		panel.add(days, c);
		
		this.add(panel);
		this.setSize(600, 600);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
	}
	
	///addC/dropC listeners
	public void addClistener(ActionListener l) {
		addClient.addActionListener(l);
	}
	
	public void dropClistener(ActionListener l) {
		dropClient.addActionListener(l);
	}
	///addP/dropP listeners
	public void addPlistener(ActionListener l) {
		addProduct.addActionListener(l);
	}
	
	public void dropPlistener(ActionListener l) {
		dropProduct.addActionListener(l);
	}
	
	///addOrderItem/createOrder listeners
	public void addOrderItemlistener(ActionListener l) {
		addOrderItem.addActionListener(l);
	}
	
	public void removeOrderItemlistener(ActionListener l) {
		dropOrderItem.addActionListener(l);
	}
	
	public void createorderistener(ActionListener l) {
		createOrder.addActionListener(l);
	}
	
	public void removeorderistener(ActionListener l) {
		deleteOrder.addActionListener(l);
	}
	
	///Client
	public String getidC() {
		String s = idC.getText();
		return s;
	}
	
	public String getfirstC() {
		String s = firstC.getText();
		return s;
	}
	
	public String getsureC() {
		String s = sureC.getText();
		return s;
	}
	
	public String getadrC() {
		String s = adrC.getText();
		return s;
	}
	
	public String getemailC() {
		String s = emailC.getText();
		return s;
	}
	
	///Product
	public String getidP() {
		String s = idP.getText();
		return s;
	}
	
	public String getnameP() {
		String s = nameP.getText();
		return s;
	}
	
	public String getpriceP() {
		String s = priceP.getText();
		return s;
	}
	
	public String getstockP() {
		String s = stockP.getText();
		return s;
	}
	
	///OrderItem
	public String getOrderItemId() {
		String s = idOrderItem.getText();
		return s;
	}
	
	public String getOrderItemIdOrders() {
		String s = idOrd.getText();
		return s;
	}
	
	public String getOrderItemIdProduct() {
		String s = idPro.getText();
		return s;
	}
	
	public String getQuantity() {
		String s = quant.getText();
		return s;
	}
	
	//Orders
	public String getOrdersId() {
		String s = idOrder.getText();
		return s;
	}
	
	public String getOrdersIdClient() {
		String s = idCll.getText();
		return s;
	}
	
	public String getOrdersPrice() {
		String s = pric.getText();
		return s;
	}
	
	public String getDays() {
		String s = days.getText();
		return s;
	}
	
	
	public JPanel getPanel() {
		return panel;
		
	}
	
	public void setClients(JTable tab) {
		clients = tab;
	}
	
	public void setProducts() {
		Product p = new Product();
		products = p.printAll("Product");
	}

}
