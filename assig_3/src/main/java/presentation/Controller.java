package presentation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JTable;

import DAO.Reflection;
import Model.Clientt;
import Model.OrderItem;
import Model.Orders;
import Model.Product;

public class Controller {
	
	private View view;
	public Controller(View v) {
		new ArrayList<String>();
		view = v;
		view.addClistener(new addC());
		view.dropClistener(new dropC());
		view.addPlistener(new addP());
		view.dropPlistener(new dropP());
		view.addOrderItemlistener(new Orderitem());
		view.removeOrderItemlistener(new removeOI());
		view.createorderistener(new createOrder());
		view.removeorderistener(new removeO());
	}
	
	public class addC implements ActionListener{

		
		public void actionPerformed(ActionEvent e) {
			Clientt c = new Clientt();
			String s = c.addClientQuery(view.getidC(), view.getfirstC(), view.getsureC(), view.getadrC(), view.getemailC());
			System.out.println(s);
			c.add(s);
			
			view = new View("Warehouse");
		}
		
	}
	
	public class dropC implements ActionListener{

		
		public void actionPerformed(ActionEvent e) {
			Clientt c = new Clientt();
			String s = c.dropClientQuery(view.getidC());
			System.out.println(s);
			c.drop(s);
			
			view = new View("Warehouse");
		}
		
	}
	
	public class addP implements ActionListener{

		
		public void actionPerformed(ActionEvent e) {
			Product p = new Product();
			String s = p.addProductQuery(view.getidP(), view.getnameP(), view.getpriceP(), view.getstockP());
			System.out.println(s);
			p.drop(s);
			
			view = new View("Warehouse");			
		}
		
	}
	
	public class dropP implements ActionListener{

		
		public void actionPerformed(ActionEvent e) {
			Product c = new Product();
			String s = c.dropProductQuery(view.getidP());
			System.out.println(s);
			c.drop(s);
			
			view = new View("Warehouse");
		}
		
	}
	
	public class Orderitem implements ActionListener{

		
		public void actionPerformed(ActionEvent e) {
			OrderItem o = new OrderItem();
			String s = o.addOrderItemQuery(view.getOrderItemId(), view.getOrderItemIdOrders(), view.getOrderItemIdProduct(), view.getQuantity());
			System.out.println(s);
			o.add(s);
			view = new View("Warehouse");
		}
		
	}
	
	public class removeOI implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			OrderItem o = new OrderItem();
			String s = o.dropOrderItemQuery(view.getOrderItemId());
			System.out.println(s);
			o.drop(s);
			view = new View("Warehouse");
		}
		
	}
	
	public class createOrder implements ActionListener{

		
		public void actionPerformed(ActionEvent e) {
			Orders o = new Orders();
			String s = o.addOrderQuery(view.getOrdersId(), view.getOrdersIdClient(), view.getOrdersPrice(), view.getDays());
			System.out.println(s);
			o.add(s);
			view = new View("Warehouse");
		}
		
	}
	
	public class removeO implements ActionListener{

		
		public void actionPerformed(ActionEvent e) {
			Orders o = new Orders();
			String s = o.dropOrderQuery(view.getOrdersId());
			System.out.println(s);
			o.drop(s);
			view = new View("Warehouse");
		}
		
	}
	
}
